extern crate libremarkable;

use libremarkable::appctx::ApplicationContext;
use libremarkable::framebuffer::common::{color, display_temp, dither_mode, waveform_mode, DRAWING_QUANT_BIT};
use libremarkable::framebuffer::PartialRefreshMode;
use libremarkable::framebuffer::{FramebufferDraw, FramebufferRefresh};

use std::{thread, time};
use cgmath::{Point2, Vector2};
use libremarkable::framebuffer::core::Framebuffer;
use libremarkable::input::InputEvent::{MultitouchEvent, WacomEvent};

use std::sync::{Arc, Mutex};
use std::time::Duration;

struct State {
    // points
    player_points: i32,
    enemy_points: i32,
}

struct BallMovementState {
    ball_pos: Point2<i32>,
    ball_speed_right: i32,
    ball_speed_up: i32,
}

fn main() {
    let mut app = ApplicationContext::default();

    hard_refresh(&mut app);

    //config /////////////////
    const SPEED_UP_RATE: i32 = 1;
    //////////////////////////

    // State
    let mut state = Arc::new(Mutex::new(State {
        player_points: 0,
        enemy_points: 0,
    }));

    let mut ball_pos = Point2 {x:100, y:100};

    // ball Movement
    let mut ball_state = Arc::new(Mutex::new(BallMovementState {
        ball_pos,
        ball_speed_right: 20,
        ball_speed_up: 20,
    }));
    // let mut ball_pos = Arc::new(Mutex::new(Point2 {x:100, y:100}));
    // let mut ball_speed_right = Arc::new(Mutex::new(20));
    // let mut ball_speed_up = Arc::new(Mutex::new(20));

    // finger platform movement
    let mut finger_cords = Point2 {x:0, y:0};

    // pen platform movement
    let mut pen_cords = Point2 {x:0, y:0};

    // framebuffer & Thread handles
    let mut fb = Arc::new(Mutex::new(app.get_framebuffer_ref()));
    let mut handles = vec![];

    app.clear(true);
    hard_refresh(&mut app);

    // Ball Thread
    // let ball_pos_ball_thread = ball_pos.clone();
    // let ball_speed_right_ball_thread = ball_speed_right.clone();
    // let ball_speed_up_ball_thread = ball_speed_up.clone();
    let ball_state_ball_thread = ball_state.clone();
    let state_ball_thread = state.clone();
    let fb_ball_thread = fb.clone();
    handles.push(thread::spawn(move || {
        println!("starting ball thread");

        let mut last_speedup = time::Instant::now();
        let mut last_update = time::Instant::now();
        let mut last_draw = time::Instant::now();
        let mut last_draw_pos = Point2 {x:0, y:0};

        loop {
            // let mut ball_speed_right = ball_speed_right_ball_thread.lock().unwrap();
            // let mut ball_speed_up = ball_speed_up_ball_thread.lock().unwrap();

            let mut ball_state = ball_state_ball_thread.lock().unwrap();
            let mut ball_speed_right = ball_state.ball_speed_right;
            let mut ball_speed_up = ball_state.ball_speed_up;

            // SpeedUp
            if time::Instant::now() - last_speedup > time::Duration::from_secs(10){
                last_speedup = time::Instant::now();
                if ball_speed_up > 0 { ball_speed_up += SPEED_UP_RATE}
                else { ball_speed_up -= SPEED_UP_RATE }

                if ball_speed_right > 0 { ball_speed_right += SPEED_UP_RATE}
                else { ball_speed_right -= SPEED_UP_RATE }

                println!("SpeedUP!! x-speed: {}, y-speed: {}", ball_speed_right, ball_speed_up);
            }

            //println!("{:?}", last_speedup);

            if time::Instant::now() - last_update > time::Duration::from_millis((500/ ball_speed_up).abs() as u64) {
                last_update = time::Instant::now();
                // let mut ball_pos = ball_pos_ball_thread.lock().unwrap();
                let ball_pos = ball_state.ball_pos;

                let mut state = state_ball_thread.lock().unwrap();
                let mut fb = fb_ball_thread.lock().unwrap();

                //println!("Ball at position before move: {}, {}", ball_pos.x, ball_pos.y);

                // Move
                ball_state.ball_pos.x += (ball_speed_right / 2);
                ball_state.ball_pos.y += (ball_speed_up / 2);

                // Handle collision
                handle_wall_collision(&mut ball_state, &mut state);
                // Draw ball
                if time::Instant::now() - last_draw > time::Duration::from_millis((1000/ ball_speed_up).abs() as u64) {
                    last_draw = time::Instant::now();
                    //println!("Drawing Ball at: {}, {}", ball_pos.x, ball_pos.y);
                    last_draw_pos = draw_ball(&mut fb, last_draw_pos, ball_pos).clone();
                }
            }
        }
    }));

    // points render thread
    let state_points_render_thread = state.clone();
    let fb_points_render_thread = fb.clone();
    handles.push(thread::spawn(move || {
        println!("starting render points thread");

        let mut last_update = time::Instant::now();
        let mut old_state = State {
            player_points: 1,
            enemy_points: 1,
        };
        loop {
            if time::Instant::now() - last_update > time::Duration::from_secs(1) {
                last_update = time::Instant::now();

                let mut state = state_points_render_thread.lock().unwrap();
                println!("current state: {}, {}", state.player_points, state.enemy_points);
                if state.player_points == old_state.player_points && state.enemy_points == old_state.enemy_points { continue; }

                old_state = State {
                    player_points: state.player_points,
                    enemy_points: state.enemy_points,
                };

                let mut fb = fb_points_render_thread.lock().unwrap();

                println!("rendering points!");
                render_points(*fb, &mut *state);
            }
        }
    }));

    let ball_state_main = ball_state.clone();
    let fb_main = fb.clone();
    let mut last_update = time::Instant::now();
    let mut last_draw_player = time::Instant::now();
    let mut last_draw_enemy = time::Instant::now();
    let mut last_drawn_cords_player = Point2 {x:0,y:0};
    let mut last_drawn_cords_enemy = Point2 {x:0,y:0};
    println!("starting main event loop!");
    app.start_event_loop(true, true, false, |ctx, event| {
        if time::Instant::now() - last_update > time::Duration::from_millis(10) {
            last_update = time::Instant::now();
            //println!("got event: {:?}", event);
            // Input Handle
            match event {
                MultitouchEvent { event: m_event } => {
                    match handle_finger_input(m_event) {
                        Some(cords) => {
                            let mut ball_state = ball_state_main.lock().unwrap();
                            let mut fb = fb_main.lock().unwrap();

                            finger_cords = cords;

                            //Draw enemy platform
                            if time::Instant::now() - last_draw_enemy > time::Duration::from_millis(200) {
                                last_draw_enemy = time::Instant::now();
                                last_drawn_cords_enemy = draw_platform(finger_cords, last_drawn_cords_enemy, *fb, 210);
                            }

                            handle_collision(&mut ball_state, pen_cords, finger_cords);
                        },
                        None => {}
                    }
                }
                WacomEvent { event: w_event } => {
                    //println!("got w_event, handling: {:?}", handle_pen_input(w_event));
                    match handle_pen_input(w_event) {
                        Some(cords) => {
                            let mut ball_state = ball_state_main.lock().unwrap();
                            let mut fb = fb_main.lock().unwrap();

                            pen_cords = cords;

                            // Draw player platform
                            if time::Instant::now() - last_draw_player > time::Duration::from_millis(200) {
                                last_draw_player = time::Instant::now();
                                last_drawn_cords_player = draw_platform(pen_cords, last_drawn_cords_player, *fb, 1510);
                            }

                            handle_collision(&mut ball_state, pen_cords, finger_cords);
                        },
                        None => {}
                    }
                }
                _ => {}
            }
        }
    });

    for handle in handles {
        handle.join().unwrap();
    }
}

fn draw_platform(pen_pos: Point2<i32>, pen_last_pos: Point2<i32>, fb: &mut Framebuffer, height: i32) -> Point2<i32> {
    let x = (pen_pos.x - 50) as i32;
    let y = height;

    println!("drawing player platform");

    // turn white
    if pen_last_pos.x - x > 5 || pen_last_pos.x - x < -5 && pen_last_pos.y != 0 {
        let region = fb.draw_line(
            pen_last_pos,
            Point2{x: pen_last_pos.x+100, y},
            10,
            color::WHITE,
        );

        refresh(fb, region, false);
    }

    // turn black
    let current_cords = cgmath::Point2 { x, y };

    let region = fb.draw_line(
        current_cords,
        Point2{x:x+100, y},
        10,
        color::BLACK,
    );

    refresh(fb, region, false);

    return Point2{x,y}
}

fn handle_wall_collision(ball_state: &mut BallMovementState, state: &mut State) {
    let ball_pos = ball_state.ball_pos;
    // walls
    if ball_pos.x > libremarkable::dimensions::DISPLAYWIDTH as i32 - 5 || ball_pos.x < 5 {
        println!("collision!! ball position: {}, {}", ball_pos.x, ball_pos.y);
        ball_state.ball_speed_right *= -1;
    }
    if ball_pos.y > libremarkable::dimensions::DISPLAYHEIGHT as i32 - 5 || ball_pos.y < 5 {
        println!("collision!! ball position: {}, {}", ball_pos.x, ball_pos.y);
        ball_state.ball_speed_up *= -1;
    }

    // goals - sets points
    if ball_pos.y > libremarkable::dimensions::DISPLAYHEIGHT as i32 - 5 {
        state.enemy_points += 1;
    }
    if ball_pos.y < 5 {
        state.player_points += 1;
    }
}

fn handle_collision(ball_state: &mut BallMovementState, player_cords: Point2<i32>, enemy_cords: Point2<i32>) {
    let ball_pos = ball_state.ball_pos;

    // player platform
    if (ball_pos.x < player_cords.x + 80 && ball_pos.x > player_cords.x - 80) && (ball_pos.y > 1490 && ball_pos.y < 1510) {
        ball_state.ball_speed_up *= -1;
    }
    // enemy platform
    if (ball_pos.x < enemy_cords.x + 80 && ball_pos.x > enemy_cords.x - 80) && (ball_pos.y > 190 && ball_pos.y < 210) {
        ball_state.ball_speed_up *= -1;
    }
}

fn handle_finger_input(event: libremarkable::input::MultitouchEvent) -> Option<Point2<i32>> {
    match event {
        libremarkable::input::MultitouchEvent::Move { finger: Finger } => {
            //print!("(finger) x: {}, y: {}", Finger.pos.x, Finger.pos.y);

            let new_pos = Point2 {
                x: Finger.pos.x as i32,
                y: Finger.pos.y as i32,
            };

            return Some(new_pos);
        }
        libremarkable::input::MultitouchEvent::Press { finger: Finger} => {
            let new_pos = Point2 {
                x: Finger.pos.x as i32,
                y: Finger.pos.y as i32,
            };

            return Some(new_pos);
        }
        _ => {}
    }

    return None;
}

fn handle_pen_input(event: libremarkable::input::WacomEvent) -> Option<Point2<i32>>{
    //println!("handling event: {:?}", event);
    match event {
        libremarkable::input::WacomEvent::Hover {
            position: cgmath::Point2 { x, y },
            distance: u16,
            tilt: _,
        } => {
            let new_position = Point2 {
                x: x as i32,
                y: y as i32
            };

            return Some(new_position);
        }
        _  => {}
    }

    return None;
}

fn render_points(mut fb: &mut Framebuffer, state: &mut State) {
    // enemy points
    let region = fb.draw_text(
        Point2{x:1300.0, y:100.0},
        &(*state).enemy_points.to_string(),
        50.0,
        color::BLACK,
        false,
    );

    refresh(fb, region, true);

    // player points
    let region = fb.draw_text(
        Point2{x:100.0, y:1772.0},
        &(*state).player_points.to_string(),
        50.0,
        color::BLACK,
        false,
    );

    refresh(fb, region, true);
}

fn draw_ball (mut fb: &mut Framebuffer, last_ball_cords: Point2<i32>, ball_cords: Point2<i32>) -> Point2<i32> {
    //println!("(ball) x: {}, y: {}", last_ball_cords.x, last_ball_cords.y);

    // turn white
    let region = fb.fill_circle(
        last_ball_cords,
        10,
        color::WHITE
    );

    refresh(fb, region, false);

    // turn black
    let region = fb.fill_circle(
        ball_cords,
        10,
        color::BLACK
    );

    refresh(fb, region, false);

    return ball_cords;
}

fn refresh(fb: &mut libremarkable::framebuffer::core::Framebuffer, region: libremarkable::framebuffer::common::mxcfb_rect, force_full: bool) {
    fb.partial_refresh(
        &region,
        PartialRefreshMode::Async,
        // DU mode only supports black and white colors.
        // See the documentation of the different waveform modes
        // for more information
        waveform_mode::WAVEFORM_MODE_DU,
        display_temp::TEMP_USE_REMARKABLE_DRAW,
        dither_mode::EPDC_FLAG_EXP1,
        DRAWING_QUANT_BIT,
        force_full,
    );
}

fn hard_refresh(app: &mut ApplicationContext) {
    // fill with black
    let fb = app.get_framebuffer_ref();
    fb.fill_rect(
        Point2{x:0,y:0},
        Vector2{
            x: libremarkable::dimensions::DISPLAYWIDTH as u32,
            y: libremarkable::dimensions::DISPLAYHEIGHT as u32,
        },
        color::BLACK
    );
    fb.full_refresh(
        // DU mode only supports black and white colors.
        // See the documentation of the different waveform modes
        // for more information
        waveform_mode::WAVEFORM_MODE_DU,
        display_temp::TEMP_USE_REMARKABLE_DRAW,
        dither_mode::EPDC_FLAG_EXP1,
        DRAWING_QUANT_BIT,
        true,
    );

    fb.fill_rect(
        Point2{x:0,y:0},
        Vector2{
            x: libremarkable::dimensions::DISPLAYWIDTH as u32,
            y: libremarkable::dimensions::DISPLAYHEIGHT as u32,
        },
        color::WHITE
    );
    fb.full_refresh(
        // DU mode only supports black and white colors.
        // See the documentation of the different waveform modes
        // for more information
        waveform_mode::WAVEFORM_MODE_DU,
        display_temp::TEMP_USE_REMARKABLE_DRAW,
        dither_mode::EPDC_FLAG_EXP1,
        DRAWING_QUANT_BIT,
        true,
    );
}