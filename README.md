# RePong
[![rm1](https://img.shields.io/badge/rm1-probably%20supported-yellow)](https://remarkable.com/store/remarkable)
[![rm2](https://img.shields.io/badge/rM2-supported-green)](https://remarkable.com/store/remarkable-2)

Two player **Pong** for the ReMarkable 2. Created in rust using [libremarkable](https://github.com/canselcik/libremarkable)

<img width="50%" src="media/game.jpg">

## Controlling
Just hover with your pen over the bottom to control the player platform
and move the enemy platform at the top with your finger on the screen.
## Installation
### Prebuilt binary/program
- Go the the [releases page](https://codeberg.org/Lesekater/RePong/releases)
- Get the newest released "RePong" file and copy it onto your remarkable, using e.g. FileZilla, WinSCP or scp.
- SSH into your remarkable and mark the file as executable with `chmod +x RePong`
- Stop xochitl (the interface) with `systemctl stop xochitl`
- Start the game with `./RePong`
- After you're done, restart xochitl with `systemctl start xochitl`
### Compiling
- Make sure to have rustup and a current toolchain (nightly might be needed)
- Install the [toolchain](https://remarkablewiki.com/devel/toolchain) or use the [toltec toolchain](https://github.com/toltec-dev/toolchain/).
 - If you're not using linux, you might want to adjust the path in `.cargo/config`
- Compile it with `make run-x` or `run-docker` (using the toltec toolchain).
