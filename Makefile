# For non-musl, use: armv7-unknown-linux-gnueabihf
TARGET ?= armv7-unknown-linux-gnueabihf

DEVICE_IP ?= '10.11.99.1'
DEVICE_HOST ?= root@$(DEVICE_IP)
FILENAME ?= 'RePong'

run-docker:
	sudo docker run --rm -v $(pwd):/project -v /project/.cargo -w "/project" ghcr.io/toltec-dev/rust:latest /bin/bash -c "cd /project && ls && cargo build --release --target=armv7-unknown-linux-gnueabihf"
	#deploy

deploy:
	ssh $(DEVICE_HOST) 'killall -q -9 demo || true; systemctl stop xochitl || true'
	ssh $(DEVICE_HOST) 'rm ./$(FILENAME) || true'
	scp ./target/$(TARGET)/release/libremarkable-test $(DEVICE_HOST):
	ssh $(DEVICE_HOST) 'RUST_BACKTRACE=1 RUST_LOG=debug ./$(FILENAME)'

deploy-x: run-x
	ssh $(DEVICE_HOST) 'killall -q -9 demo || true; systemctl stop xochitl || true'
	ssh $(DEVICE_HOST) 'rm ./$(FILENAME) || true'
	scp ./target/$(TARGET)/release/$(FILENAME) $(DEVICE_HOST):
	ssh $(DEVICE_HOST) 'RUST_BACKTRACE=1 RUST_LOG=debug ./$(FILENAME)'

run-x:
	cross build --release --target=armv7-unknown-linux-gnueabihf